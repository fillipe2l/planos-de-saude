import React, { Component, Fragment } from 'react';
import { obterPlanos } from '../../services/api';
import Loading from '../Loading';
import { Row, Col, Card, Button, Modal, Badge, Input } from 'react-materialize';
import "./planos.css";
import { Route, Redirect } from 'react-router-dom';

class Planos extends Component {

  constructor(props) {
    super(props);
    this.state = {
      plains_finded: [],
      loading: false,
      filter: {uf: 'DF'},
      firstFind: true,
      plain_details: [],
      user: {name: '', email: ''},
      form: { touched: false, valid: false, submitted: false },
      invalidEmail: false
    }
    this.handleClick = this.handleClick.bind(this);
  }

  /** FUNÇÃO QUE FILTRA O ARRAY DE PLANOS COM A UF PREVIAMENTE SELECIONADA */
  handleClick() {
    this.setState({ loading: true});
    obterPlanos().then((dados) => {
      this.setState({plains_finded:
        dados.filter((res) => {
          this.setState({ loading: false, firstFind: false });
          return res.uf.toUpperCase() === this.state.filter.uf.toUpperCase()
        })
      });
    });
  }

  /** MÉTODO QUE DEFINE QUAL UF SERÁ BUSCADA NO ARRAY DE PLANOS
   * @newValue é o novo valor que será definido para a busca
  */
  changeFilter(newValue) {
    this.setState({filter: {uf: newValue}});
  }

  /** MÉTODO PARA SELECIONAR QUAL ELEMENTO DO ARRAY DE PLANOS SERÁ EXIBIDO NA MODAL
   * @row é o elemento com as informações que serão mostradas na modal
  */
  showModal(row) {
    this.setState({plain_details: row});
  }

  /** VALIDA SE OS CAMPOS ESTÃO PREENCHIDOS */
  handleValidate(e) {
    e.preventDefault();
    let input = e.target;
    this.state.user[`${input.id}`] = input.value;
    let { name, email } = this.state.user;
    this.setState({ form: { valid: (name.length > 0 && email.length > 0), submitted: false}, invalidEmail: email.length > 0 && !this.testEmail()});
    input.value.length > 0 ? input.className = '' : input.className = 'error';
  }

  /** TESTA SE O FORMULÁRIO ESTÁ VÁLIDO E, CONSEQUENTEMENTE, MARACA QUE FOI SUBMETIDO */
  handleSubmit = async() => {
    window.event.preventDefault();
    let emailValido = this.testEmail();
    if(emailValido){
      this.setState(
        {
          form: {
            valid: emailValido,
            submitted: emailValido
          },
          invalidEmail: !this.testEmail()
        }
      )
    } else {
      document.querySelector("input[id='email']").className = "error";
      this.setState({user: {name: this.state.user.name, email: ''}, form: {valid: false, submitted: false}, invalidEmail: true});
    }
  }
  
  /** VALIDA SE O EMAIL DIGITADO PELO USUÁRIO É VÁLIDO */
  testEmail() {
    let email = this.state.user.email;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  render() {
    const { loading } = this.state;
    return (
        <div>
            {/* ELEMENTOS PARA A BUSCA */}
            <Card>
              <Row>
                  <Col m={8} s={12} align="right" className="group-select mb-10">
                    {/* SELECT COM OS ESTADOS DISPONÍVEIS PARA A BUSCA */}
                    <select className="browser-default personal-select" defaultValue="DF" onChange={(e) => this.changeFilter(e.target.value)}>
                          <option value="AC">Acre</option>
                          <option value="AL">Alagoas</option>
                          <option value="AP">Amapá</option>
                          <option value="AM">Amazonas</option>
                          <option value="BA">Bahia</option>
                          <option value="CE">Ceará</option>
                          <option value="DF">Distrito Federal</option>
                          <option value="ES">Espírito Santo</option>
                          <option value="GO">Goiás</option>
                          <option value="MA">Maranhão</option>
                          <option value="MT">Mato Grosso</option>
                          <option value="MS">Mato Grosso do Sul</option>
                          <option value="MG">Minas Gerais</option>
                          <option value="PA">Pará</option>
                          <option value="PB">Paraíba</option>
                          <option value="PR">Paraná</option>
                          <option value="PE">Pernambuco</option>
                          <option value="PI">Piauí</option>
                          <option value="RJ">Rio de Janeiro</option>
                          <option value="RN">Rio Grande do Norte</option>
                          <option value="RS">Rio Grande do Sul</option>
                          <option value="RO">Rondônia</option>
                          <option value="RR">Roraima</option>
                          <option value="SC">Santa Catarina</option>
                          <option value="SP">São Paulo</option>
                          <option value="SE">Sergipe</option>
                          <option value="TO">Tocantins</option>
                    </select>
                  </Col>
                  <Col m={4} s={12} align="left" className="group-button mb-10">
                    <Button className="btn btn-full-width" onClick={this.handleClick}>
                      buscar planos
                    </Button>
                  </Col>

                  {/* VALIDAÇÃO VISUAL PARA DEMMONSTRAR QUANTOS RESULTADOS A BUSCA RETORNOU */}
                  { this.state.plains_finded.length > 0 ? 
                  <Col m={12} s={12}>
                      <Badge caption="more" className="mt-10">
                        { this.state.plains_finded.length > 1 ? this.state.plains_finded.length + ' planos encontrados' : this.state.plains_finded.length + ' plano encontrado'}
                      </Badge>
                  </Col> :
                  null
                }
              </Row>
            </Card>
            <Row>
                {/* EXIBIÇÃO DOS RESULTADOS RETORNADOS DO ARRAY DE PLANOS */}
                { this.state.plains_finded.map((row, i) =>
                  <Col m={12} s={12} key={i}>
                    <Card>
                      <Row>
                        <Col m={7} s={12}>
                          { row.razao_social }
                        </Col>
                        <Col m={3} s={12}>
                        <b>{ row.cidade }, { row.uf }</b>
                        </Col>
                        <Col m={2} s={12} align="right">
                          {/* BOTÃO QUE INVOCA A MODAL E PASSA A ROW(ELEMENTO DO ARRAY COM AS INFO. DO PLANO) COMO PARAMETRO */}
                          <Button data-target="modal1" className="btn-default modal-trigger" onClick={() => { this.showModal(row)}}>DETALHES</Button>
                        </Col>
                      </Row>
                    </Card>
                  </Col>
                  )
                }
            </Row>
            <Row align="center">
                {/* ESTADO INICIAL. USUÁRIO ENTROU NA TELA MAS NÃO EFETUOU BUSCAS AINDA */}
                { this.state.firstFind ? 
                  <Col m={12} s={12}>
                    <Card>
                      <h5> Aqui você pode buscar planos mais próximos de você </h5>
                      <h6>Selecione o seu estado e clique em buscar planos para começar</h6>
                    </Card>
                  </Col>
                : null
                }
                
                {/* USUARIO EFETUOU BUSCA MAS NÃO SEM RESULTADO NO ARRAY */}
                { !this.state.firstFind && this.state.plains_finded.length < 1 ? 
                  <Col m={12} s={12}>
                    <Card>
                      <h5> Nenhum plano disponível ainda nessa região :( </h5>
                    </Card>
                  </Col>
                : null
                }
            </Row>

            {/* ELEMENTO QUE MOSTRA QUE A PÁGINA ESTÁ SOFRENDO UMA CARGA / LOADING */}
            <Fragment>
              <Loading loading={loading} message='' />
            </Fragment>

            {/* ELEMENTO MODAL, MOSTRADA SE... */}
            { (this.state.form.submitted === false)
            ? 
            // ESSA SE AINDA NÃO INFORMOU NOME E EMAIL
            <Modal
                actions={[
                  <Button flat modal="close">Cancelar e não ver planos</Button>
                ]} fixedFooter id="modal1">
                  <h4><center>Um momento antes...</center></h4>
              <center>Informe seu nome e email para ver mais detalhes! É rápido e fácil...</center>
              <Card>
                <form>
                  <Row>
                      <Col m={12} l={6} className="full-width">
                        <input autoComplete={'off'} placeholder="Nome" id="name" onFocus={(e) => this.handleValidate(e)} onBlur={(e) => this.handleValidate(e)} onChange={(e) => this.handleValidate(e)}/>
                      </Col>
                      <Col m={12} l={6} className="full-width">
                        <input autoComplete={'off'} placeholder="E-mail" id="email" onClick={(e) => this.handleValidate(e)} onBlur={(e) => this.handleValidate(e)} onChange={(e) => this.handleValidate(e)}/>
                        { this.state.invalidEmail
                        ?
                        <span className="validator-error">Digite um e-mail válido (ex: joao@hotmail.com)</span>
                        : null
                        }
                      </Col>
                  </Row>
                  <Row align="right">
                    <Col m={12} l={12} className="full-width">
                      <Button onClick={this.handleSubmit} type="submit" disabled={!this.state.form.valid}>CONFIRMAR</Button>
                    </Col>
                  </Row>
                </form>
              </Card>
            </Modal>
            :
            this.props.history.push({pathname: '/info-plano', data: this.state.plain_details})
          }
        </div>
    ) 
  }
}

export default Planos;