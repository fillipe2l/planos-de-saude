// Importando o React
import React from "react";
// Importando os components necessários da lib react-materialize
import { Row, Col, Card, Button, Slider, Slide } from 'react-materialize';
import './inicio.css';
import { NavLink } from 'react-router-dom'
// import Img1 from "https://cetic.br/media/imgs/tic/TICsaude-big.jpg";

const Inicio = () => (
  <div>
    {/* ELEMENTO QUE EXIBE OS SLIDES */}
    <Slider>
        <Slide src="https://cetic.br/media/imgs/tic/TICsaude-big.jpg">
          <div align="left">SUS</div>
        </Slide>
        <Slide src="https://www.anubisseguros.com.br/wp-content/uploads/2019/08/PLANO-DE-SA%C3%9ADE-1200x565.jpg">
        <div align="left">SUS</div>
        </Slide>
        <Slide src="https://accecontabilidade.com.br/wp-content/uploads/2017/09/Microempreendedor-Individuaal.jpg">
        <div align="left">SUS</div>
        </Slide>
    </Slider>
    <Row>

      <Col m={12} s={12}>
          <h5 className="subtitle">Sobre Planos de Saúde</h5>
          <Card>
            <div>
              <p><b>Aqui você vai encontrar os Planos de Saúde que mais combinam com você</b></p>
              <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laborevoluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
              <br/>
              <div align="center">
                <NavLink to="planos"><Button className="btn-default">VEJA OS PLANOS DISPONÍVEIS</Button></NavLink>
              </div>
            </div>
          </Card>
      </Col>
    </Row>
  </div>
);

export default Inicio;