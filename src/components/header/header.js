// Importando o React
import React from "react";
// Importando os components necessários da lib react-materialize
import { Navbar, Row, Col, Icon} from 'react-materialize';
import './header.css';
import { NavLink } from 'react-router-dom'
import Logo from '../../imagens/logo-saude-exemplo.png';

const Header = () => (
  <Row>
    <div className="sup-header">
      <Row className="mb-4">
        <Col m={6} l={6} className="sup-header-tel-info"><Icon tiny>phone</Icon>&nbsp;(61) 9 9876-5432 / (61) 3001-0123</Col>
        <Col m={6} l={6} align="left" className="sup-header-extra-info">Busque planos de saúde onde você estiver</Col>
      </Row>
    </div>
    <Navbar className="header">
      <li className="li-logo"><img className="logo" src={Logo} /></li>
      <li><NavLink to="/" className="page-link">Início</NavLink></li>
      <li><span className="separator">|</span></li>
      <li><NavLink to="planos" className="page-link">Planos</NavLink></li>
      <li><span className="separator">|</span></li>
      <li><NavLink to="#" className="page-link">Sobre nós</NavLink></li>
    </Navbar>
  </Row>
);

export default Header;