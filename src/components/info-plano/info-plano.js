import React, { Component, Fragment } from 'react';
import { Row, Col, Card, Button, Modal, Badge, Input } from 'react-materialize';
import { NavLink } from 'react-router-dom'
import './info-plano.css';

class InfoPlano extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if(this.props.location.data == undefined){
            this.props.history.push({pathname: '/planos'})
        }
        console.log(this.props.location.data);
    }

    /** REMOÇÃO DA MODAL QUE SOBREPÕE AS PÁGINAS */
    removeModal = () => {
        if(document.querySelector(".modal-overlay"))
            document.querySelector(".modal-overlay").remove();
    }


    render () {
        this.removeModal();
        const { bairro, cargo_representante, cep,
                cidade, cnpj, complemento, data_registro,
                ddd, email, fax, id, logradouro,
                modalidade, nome_fantasia, numero,
                razao_social, representante, telefone, uf }  = this.props.location.data || [];
        return (
        <div>
                <Card>
                    <Row>
                            <Col align="center" m={12} l={12} className="full-width">
                                <h5>{ razao_social }
                                <br/>
                                ({modalidade})</h5>
                            </Col>
                    </Row>
                    <Row>
                            <Col align="center" m={12} l={6} className="full-width">
                                <h6>{ bairro }, { cidade }, { uf } ({ logradouro }, { numero } - { cep })</h6>
                            </Col>
                            <Col align="center" m={12} l={6} className="full-width">
                                <h6><b>Telefone:</b> { telefone } | <b>Fax:</b> { fax } | <b>Email:</b> { email }</h6>
                            </Col>
                    </Row>
                    <hr/>
                    <Row>
                            <Col align="center" m={12} l={12} className="full-width">
                                Quando contatar a empresa, você deve falar com <span className="captalize">{ representante }</span> para tirar suas dúvidas.
                            </Col>
                    </Row>
                    <div className="mt-20" align="center">
                    <NavLink to="planos"><Button className="btn-default">VOLTAR E VER MAIS</Button></NavLink>
                    </div>
                </Card>
        </div>
        )
    }

}

export default InfoPlano;