// Importando o React
import React, { Component, Fragment } from 'react';
// Importando o Component Header
import Header from './components/header/header'
import Footer from './components/footer/footer'
import Main from './main'
import './App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Main />
        <Footer />
      </Fragment>
    );
  }
}

export default App;