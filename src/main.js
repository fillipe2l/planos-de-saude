import React from "react";
import Inicio from "./components/inicio/inicio";
import Planos from "./components/planos/planos";
import { Container } from 'react-materialize';
import { Switch, Route } from 'react-router-dom'
import InfoPlanos from "./components/info-plano/info-plano";

const Main = () => (
  <main>
    <Container>
      <Switch>
        <Route exact path='/' component={Inicio}/>
        <Route path='/planos' component={Planos}/>
        <Route path='/info-plano' component={InfoPlanos}/>
      </Switch>
    </Container>
  </main>  
);

export default Main;