import axios from 'axios';

export const API_URL = `https://testapi.io/api/lucaswx2`;

export const obterPlanos = async() => {
        return new Promise((resolve) => {
            axios.get(`${API_URL}/planos`).then((resp) => {
                resolve(resp.data);
            });
        }) 
}